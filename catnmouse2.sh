#!/bin/bash

DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF
guess=0
while [ "$guess" -ne "$THE_NUMBER_IM_THINKING_OF" ]
do
echo OK cat, I’m thinking of a number from 1 to $THE_MAX_VALUE. Make a guess:
read guess
if [ 1 -gt "$guess" ]
then
   echo "You must enter a number that’s >= 1"

elif [ "$guess" -gt "$THE_MAX_VALUE" ]
then
   echo "You must enter a number that’s <= $THE_MAX_VALUE"

elif [ "$guess" -gt "$THE_NUMBER_IM_THINKING_OF" ]
then
   echo "No cat... the number I’m thinking of is smaller than $guess"

elif [ "$guess" -lt "$THE_NUMBER_IM_THINKING_OF" ]
then
  echo "No cat... the number I’m thinking of is larger than $guess"
fi
done

echo "You got me."
echo "  /\___/\\"
echo " |       |"
echo "_  *   *  _"
echo "-   /_\   -"
echo "    ---"
