echo I am
whoami
echo The current working directory is
pwd
echo The system I am on is
hostname
echo The Linux version is
uname -r
echo The Linux distribution is
grep 'Fedora Linux 35 (Thirty Five)' -o /etc/os-release
echo The system has been up for
uptime
echo The amount of disk space I\'m using in KB is
du -k ~ | tail -1
